#include <mpi.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define SIZE 14400 /*Size of an array*/
#define EPSILON 0.0000001 /*Epsilon*/

double* matrOnVec(const double* matr, double* vec, int rank, int size, int strokes){
        double *res =(double*)malloc(sizeof(double)*strokes);
        double *xtemp = (double*)malloc(sizeof(double)*strokes);
	int i,j,s;
	for (i = 0; i < strokes; i++)
		res[i] = 0;
	int shift = rank * strokes;
	memcpy(xtemp, vec, sizeof(double)*strokes);
	for(s = 0; s < size; s++){
		for(i = 0; i < strokes; i++){
			for (j = 0 ; j < strokes; j++)
				res[i] += matr[i*SIZE + j + shift] * xtemp[j];
		}
		MPI_Sendrecv(xtemp, strokes, MPI_DOUBLE, (rank + 1) % size, 2019, 
			     vec, strokes, MPI_DOUBLE, (rank + size - 1) % size, 2019, 
			     MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		memcpy(xtemp, vec, sizeof(double)*strokes);
		shift = (shift + strokes) % SIZE;
	}
	free(xtemp);
	return res;
}

double* scalOnVec(const double scal,const double* vec, int strokes){
        double *res =(double*) malloc(sizeof(double)*strokes);
        int i;
        for(i = 0 ; i < strokes; i++)                                                          
                res[i] = vec[i]*scal;                                                          
        return res;                                                                            
}                                                                                              

/* res = vec1 - vec2 */                                                                        
double* vecMinusVec(const double* vec1,const double* vec2, int strokes){
        double *res =(double*)malloc(sizeof(double)*strokes);
        int i;
        for (i = 0; i < strokes; i++)
                res[i] = vec1[i] - vec2[i];
        return res;
}

double normOfVec(const double* vec, int strokes){
        double nr = 0;
        int i;
        for (i = 0; i < strokes; i++)
                nr += vec[i]*vec[i];
        return sqrt(nr);
}

/*Solving Ax = b
 ** A = SIZE x SIZE matrix of double
 ** x, b vectors of double SIZE-len
 ** x^(n+1) = x^n - t(Ax^n - b)
 ** t = 0.01 or -0.01
 ** |Ax^n - b|/|b| < EPSILON - criteria of an exit
 ** |x| = sqrt(sum of x_i^2)
 */

double* SimpleIter(const double* A, const double* b, int size, int rank, int strokes){
        double *res, *norm;
        res = (double*)malloc(sizeof(double)*strokes);
        if (rank == 0)
                norm = (double*)malloc(sizeof(double)*SIZE);
        double *x;
        double *_x;
        double *_y;
        double t = 0.0001;
        double z;
        int i;
        if (rank == 0)
                for(i = 0 ; i < strokes; i++)
                        res[i] = 0;
        MPI_Bcast(res, strokes, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        do{
                _x = matrOnVec(A, res, rank, size, strokes);
                _y = vecMinusVec(_x, b, strokes);
                free(_x);
                _x = scalOnVec(t, _y, strokes);
                x = vecMinusVec(res, _x, strokes); /*x^(n+1)*/
		memcpy(res, x, sizeof(double)*strokes);
                MPI_Gather(_y, strokes, MPI_DOUBLE, norm, strokes, MPI_DOUBLE, 0, MPI_COMM_WORLD);
                free(x);
                free(_x);
                free(_y);
                if (rank == 0){
                        z = normOfVec(norm, SIZE)/normOfVec(b, SIZE);
                }
                MPI_Bcast(&z, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
                if (rank == 0)
                        printf("z = %lf\n", z);
        }while(z > EPSILON);
        printf("%d proc left cycle\n", rank);
        return res;
}


int main(){
        int size, rank;
        MPI_Init(NULL, NULL);
        MPI_Comm_size(MPI_COMM_WORLD,&size);
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
        int strokes = SIZE/size;
        double *A =( double*)malloc(sizeof(double)*strokes*SIZE);// SIZE/size strokes x SIZE columns matrix
        double *b = (double*)malloc(sizeof(double)*SIZE);
        int i,j;
        for(i = 0; i < strokes; i++)
                for(j = 0; j < SIZE; j++)
                        if (i+(strokes*rank)==j)
                                A[i*SIZE + j] = 2;
                        else
                                A[i*SIZE + j] = 1;
        if (rank == 0)
                for(i = 0; i < SIZE; i++){
                        b[i] = SIZE + 1;
                        printf("b[%d] = %f\n", i, b[i]);
                }
        double *bb = (double*)malloc(sizeof(double)*strokes);
        MPI_Scatter(b, strokes, MPI_DOUBLE, bb, strokes, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        //MPI_Bcast(b, SIZE, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        //printf("%f\n", b[0]);
        /*Init A and b*/
	free(b);
	double *res = SimpleIter(A, bb, size, rank, strokes);
	free(A);free(bb);
	MPI_Barrier(MPI_COMM_WORLD);
	for (j = 0; j < size; j++){
		if (rank == j)
			for (i = 0; i < strokes; i++)
				printf("res[%d] = %f\n",i+rank*strokes, res[i]);
		MPI_Barrier(MPI_COMM_WORLD);
	}
	MPI_Finalize();
	return 0;
}

