#include <mpi.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define SIZE 14400 /*Size of an array*/
#define EPSILON 0.00000001 /*Epsilon*/

double* matrOnVec(const double* matr,const double* vec, int strokes){
	double *res =(double*) malloc(sizeof(double)*strokes);
	int i,j;
	for(i = 0; i < strokes; i++){
		res[i] = 0;
		for (j = 0; j < SIZE; j++){
			res[i] += matr[i*SIZE + j] * vec[j];
		}
	}
	return res;
}

double* scalOnVec(const double scal,const double* vec, int strokes){
	double *res =(double*) malloc(sizeof(double)*strokes);
	int i;
	for(i = 0 ; i < strokes; i++)                                                          
		res[i] = vec[i]*scal;                                                          
	return res;                                                                            
}                                                                                              

/* res = vec1 - vec2 */                                                                        
double* vecMinusVec(const double* vec1,const double* vec2, int strokes){
	double *res =(double*)malloc(sizeof(double)*strokes);
	int i;
	for (i = 0; i < strokes; i++)
		res[i] = vec1[i] - vec2[i];
	return res;
}

double normOfVec(const double* vec, int strokes){
	double nr = 0;
	int i;
	for (i = 0; i < strokes; i++)
		nr += vec[i]*vec[i];
	return sqrt(nr);
}

/*Solving Ax = b
 ** A = SIZE x SIZE matrix of double
 ** x, b vectors of double SIZE-len
 ** x^(n+1) = x^n - t(Ax^n - b)
 ** t = 0.01 or -0.01
 ** |Ax^n - b|/|b| < EPSILON - criteria of an exit
 ** |x| = sqrt(sum of x_i^2)
 */

double* SimpleIter(const double* A, const double* b, int size, int rank, int strokes){
	double *res, *norm;
	//if (rank == 0){
	res = (double*)malloc(sizeof(double)*SIZE);
	if (rank == 0)
		norm = (double*)malloc(sizeof(double)*SIZE);
	//}
	//double* act = (double*)malloc(sizeof(double)*strokes);
	double *x;
	double *_x;
	double *_y;
	double t = 0.0001;
	printf("%f\n", t);
	double z;
	int i;
	if (rank == 0)
		for(i = 0 ; i < SIZE; i++)
			res[i] = 0;
	do{
		MPI_Bcast(res, SIZE, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		_x = matrOnVec(A, res, strokes);
		_y = vecMinusVec(_x, b, strokes);
		free(_x);
		_x = scalOnVec(t, _y, strokes);
		x = vecMinusVec(res, _x, strokes); /*x^(n+1)*/
		MPI_Gather(x, strokes, MPI_DOUBLE, norm, strokes, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		if (rank == 0)
			for (i = 0; i < SIZE; i++){
				res[i] = norm[i];
				//printf("%f - %f = %f\n",b[i], res[i], b[i] - res[i]);
				//printf("res[%d] = %f\n",i , res[i]);
			}
		MPI_Gather(_y, strokes, MPI_DOUBLE, norm, strokes, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		free(x);
		free(_x);
		free(_y);
		if (rank == 0){
			z = normOfVec(norm, SIZE)/normOfVec(b, SIZE);
		}
		MPI_Bcast(&z, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
		if (rank == 0)
			printf("z = %lf\n", z);
	}while(z > EPSILON);
	printf("%d proc left cycle\n", rank);
	return res;
}


int main(){
	int size, rank;
	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD,&size);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	int strokes = SIZE/size;
	double *A =( double*)malloc(sizeof(double)*strokes*SIZE);// SIZE/size strokes x SIZE columns matrix
	double *b = (double*)malloc(sizeof(double)*SIZE);
	int i,j;
	//srand(2018);
	for(i = 0; i < strokes; i++)
		for(j = 0; j < SIZE; j++)
			if (i+(strokes*rank)==j)
				A[i*SIZE + j] = 2;
			else
				A[i*SIZE + j] = 1;
	if (rank == 0)
		for(i = 0; i < SIZE; i++){
			b[i] = SIZE + 1;
			printf("b[%d] = %f\n", i, b[i]);
		}
	MPI_Bcast(b, SIZE, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	//printf("%f\n", b[0]);
	/*Init A and b*/
	double *res = SimpleIter(A, b, size, rank, strokes);
	free(A);free(b);
	MPI_Barrier(MPI_COMM_WORLD);
	if (rank == 0){
		for (i = 0; i < SIZE; i++)
			printf("x[%d] = %f\n", i, res[i]);
	}
	//for (i = 0; i < N; i++)
	//        printf("%f\n", res[i]);
	MPI_Finalize();
	return 0;
}

