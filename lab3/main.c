#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#define N1 512
#define N2 512
#define N3 512

void Transpose(double* matr, int n1, int n2){
	double* temp = (double*)malloc(n1 * n2 * sizeof(double));
	int i,j;

	for (i = 0; i < n1 * n2; i++)
		temp[i] = matr[i];
	for (i = 0; i < n1; i++)
		for (j = 0; j < n2; j++)
			matr[j * n1 + i] = temp[i * n2 + j];
	free(temp);
}

/* 
** a is n1 x n2 matrix
** b is n2 x n3 matrix
** c is n1 x n3 matrix
*/
void MultiplyMatrixes(const double *a, const double *b, double *res, size_t n1, size_t n2, size_t n3) {
	int i,j,k;
	for (i = 0; i < n1; i++) {
		for (j = 0; j < n3; j++) {
			res[i * n3 + j] = 0;
			for (k = 0; k < n2; k++)
				res[i * n3 + j] += a[i * n2 + k] * b[k * n3 + j];
		}
	}
}

int main(int argc, char **argv) {

	MPI_Init(&argc, &argv);

	int root = 0;

	int dims[2] = {0, 0}, 
	    periods[2] = {0, 0}, 
	    col[2] = {1, 0}, 
	    row[2] = {0, 1}, 
	    coords[2], 
	    reorder = 1;

	int size, rank, sizey, sizex, ranky, rankx;
	int prevy, prevx, nexty, nextx;

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Dims_create(size, 2, dims);

	sizey = dims[0];
	sizex = dims[1];

	MPI_Comm comm2d;
	MPI_Comm comm_row;
	MPI_Comm comm_column;

	MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, reorder, &comm2d);
	MPI_Cart_sub(comm2d, row, &comm_row);
	MPI_Cart_sub(comm2d, col, &comm_column);
	MPI_Cart_get(comm2d, 2, dims, periods, coords);
	ranky = coords[0];
	rankx = coords[1];
	MPI_Cart_shift(comm2d, 0, 1, &prevy, &nexty);
	MPI_Cart_shift(comm2d, 1, 1, &prevx, &nextx);

	MPI_Comm_rank(comm2d, &rank);

	double *aFull, *bFull, *cFull, *aPart, *bPart, *cPart, *cGather;
	int m1 = N1 / sizey;
	int m3 = N3 / sizex;

	aPart = (double*)malloc(m1 * N2 * sizeof(double));
	bPart = (double*)malloc(N2 * m3 * sizeof(double));
	cPart = (double*)malloc(m1 * m3 * sizeof(double));
	int i,j;
	/*Init matrixes*/
	if (rank == root){
		aFull = (double*)malloc(N1 * N2 * sizeof(double));
		bFull = (double*)malloc(N2 * N3 * sizeof(double));
		cFull = (double*)malloc(N1 * N3 * sizeof(double));
		for (i = 0; i < N1; i++)
			for (j = 0; j < N2; j++)
				aFull[i * N2 + j] = i + 1;
		for (i = 0; i < N2; i++)
			for (j = 0; j < N3; j++)
				if (i == j)
					bFull[i * N3 + j] = 1;
				else
					bFull[i * N3 + j] = 0;
		Transpose(bFull, N2, N3);
	}/*Init matrixes*/
	/*Send info to everyone*/
	if (rankx == 0){
		MPI_Scatter(aFull, m1 * N2, MPI_DOUBLE, 
				aPart, m1 * N2, MPI_DOUBLE, 
				0, comm_column);
		cGather = (double*)malloc(m1 * N3 * sizeof(double));
	}
	if (ranky == 0){
		MPI_Scatter(bFull, N2 * m3, MPI_DOUBLE, 
				bPart, N2 * m3, MPI_DOUBLE, 
				0, comm_row);
	}
	MPI_Bcast(aPart, m1 * N2, MPI_DOUBLE, 0, comm_row);
	MPI_Bcast(bPart, m3 * N2, MPI_DOUBLE, 0, comm_column);
	/*End of sending info*/	
	/*Calculation*/
	Transpose(bPart, m3, N2);
	MultiplyMatrixes(aPart, bPart, cPart, m1, N2, m3);
	/*End of calculation*/
	/*Gather result*/
	for (i = 0; i < m1; i++)
		MPI_Gather(cPart + i * m3, m3, MPI_DOUBLE, 
			   cGather + i * m3 * sizex, m3, MPI_DOUBLE, 
			   0, comm_row);
	if (rankx == 0)
		MPI_Gather(cGather, m1 * N3, MPI_DOUBLE, 
			   cFull, m1 * N3, MPI_DOUBLE, 
			   0, comm_column);
	/*End of gathering*/
	/*Print result*/
	/*if (rank == root){
		for (i = 0; i < N1; i++){
			for (j = 0; j < N3; j++)
				printf("%5.2lf ", cFull[i * N3 + j]);
			printf("\n");
		}
	}*/
	MPI_Finalize();
	return 0;
}
