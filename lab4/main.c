#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <mpi.h>

/*Зависимость дельта от размера сетки, !!не забыть прогнать!!*/
/*Threads: 16
in = 150
jn = 150
kn = 150
Max differ = 2.921942e-09
Threads: 16
in = 125
jn = 125
kn = 125
Max differ = 4.470394e-09
Threads: 16
in = 100
jn = 100
kn = 100
Max differ = 1.955650e-09
Threads: 16
in = 75
jn = 75
kn = 75
Max differ = 1.062002e-09
Threads: 16
in = 50
jn = 50
kn = 50
Max differ = 7.771401e-10
Threads: 16
in = 25
jn = 25
kn = 25
Max differ = 0.000000e+00
hpcuser11@clu:~/code/lab4> cat submit.sh.o792365 
Threads: 16
in = 350
jn = 350
kn = 350
Max differ = 9.708122e-09
*/
#define in 32
#define jn 32
#define kn 32
#define a 100000
#define X 2.0
#define Y 2.0
#define Z 2.0
#define EPSILON 0.00000001

double Phi(double, double, double);

double Ro(double, double, double);

void Init(const int *perThreads, const int *offsets, const int threadRank);

void calcEdges();

void sendData();

void calcCenter();

void recData();

void findMaxDiff();

double *F[2];

double *buffer[2];
double hx, hy, hz;                                                                             
double Fi, Fj, Fk, F1;                                                                         
double owx, owy, owz;                                                                          
double c;                                                                                      
                                                                                               
int I;                                                                                         
int J;
int K;
int L0 = 1;
int L1 = 0;
int f, tmpF;
int threadRank = 0;

int threadCount = 0;
int *perThread;
double diff;
int *offset;
MPI_Request sendRequest[2] = {};
MPI_Request recRequest[2] = {};

double Phi(double x, double y, double z) {
    return x*x + y*y + z*z;
}

double Ro(double x, double y, double z) {
    return (6 - a * Phi(x, y, z));
}

void Init(const int *perThreads, const int *offsets, const int threadRank) {
	int i, j, k;
	int startLine;
    for (i = 0, startLine = offsets[threadRank]; i <= perThreads[threadRank] - 1; i++, startLine++) {
        for (j = 0; j <= jn; j++) {
            for (k = 0; k <= kn; k++) {
                if ((startLine != 0) && (j != 0) && (k != 0) && (startLine != in) && (j != jn) && (k != kn)) {
                    F[0][i * J * K + j * K + k] = 0;
                    F[1][i * J * K + j * K + k] = 0;
                } else {
                    F[0][i * J * K + j * K + k] = Phi(startLine * hx, j * hy, k * hz);
                    F[1][i * J * K + j * K + k] = Phi(startLine * hx, j * hy, k * hz);
                }
            }
        }
    }

}

void calcEdges() {
	int k, j;
    for (j = 1; j < jn; ++j) {
        for (k = 1; k < kn; ++k) {

            if (threadRank != 0) {
                int i = 0;
                Fi = (F[L0][(i + 1) * J * K + j * K + k] + buffer[0][j * K + k]) / owx;
                Fj = (F[L0][i * J * K + (j + 1) * K + k] + F[L0][i * J * K + (j - 1) * K + k]) / owy;
                Fk = (F[L0][i * J * K + j * K + (k + 1)] + F[L0][i * J * K + j * K + (k - 1)]) / owz;
		diff = fabs(F[L1][i * J * K + j * K + k] - (Fi + Fj + Fk - Ro((i + offset[threadRank]) * hx, j * hy, k * hz)) / c);
                F[L1][i * J * K + j * K + k] = (Fi + Fj + Fk - Ro((i + offset[threadRank]) * hx, j * hy, k * hz)) / c;
                if (diff > EPSILON) {
                    f = 0;
                }
            }
            if (threadRank != threadCount - 1) {
                int i = perThread[threadRank] - 1;
                Fi = (buffer[1][j * K + k] + F[L0][(i - 1) * J * K + j * K + k]) / owx;
                Fj = (F[L0][i * J * K + (j + 1) * K + k] + F[L0][i * J * K + (j - 1) * K + k]) / owy;
                Fk = (F[L0][i * J * K + j * K + (k + 1)] + F[L0][i * J * K + j * K + (k - 1)]) / owz;
		diff = fabs(F[L1][i * J * K + j * K + k] - (Fi + Fj + Fk - Ro((i + offset[threadRank]) * hx, j * hy, k * hz)) / c);
                F[L1][i * J * K + j * K + k] = (Fi + Fj + Fk - Ro((i + offset[threadRank]) * hx, j * hy, k * hz)) / c;
                if (diff > EPSILON) {
                    f = 0;
                }
            }

        }
    }
}

void sendData() {
    if (threadRank != 0) {
        MPI_Isend(&(F[L0][0]), K * J, MPI_DOUBLE, threadRank - 1, 0, MPI_COMM_WORLD, &sendRequest[0]);
        MPI_Irecv(buffer[0], K * J, MPI_DOUBLE, threadRank - 1, 1, MPI_COMM_WORLD, &recRequest[1]);
    }
    if (threadRank != threadCount - 1) { //0
        MPI_Isend(&(F[L0][(perThread[threadRank] - 1) * J * K]), K * J, MPI_DOUBLE, threadRank + 1, 1, MPI_COMM_WORLD,
                  &sendRequest[1]); //верх
        MPI_Irecv(buffer[1], K * J, MPI_DOUBLE, threadRank + 1, 0, MPI_COMM_WORLD, &recRequest[0]);
    }
}

void calcCenter() {
	int i, j, k;
    for (i = 0; i < perThread[threadRank]; i++) {
        for (j = 1; j < jn; j++) {
            for (k = 1; k < kn; k++) {
                Fi = (F[L0][(i + 1) * J * K + j * K + k] + F[L0][(i - 1) * J * K + j * K + k]) / owx;
                Fj = (F[L0][i * J * K + (j + 1) * K + k] + F[L0][i * J * K + (j - 1) * K + k]) / owy;
                Fk = (F[L0][i * J * K + j * K + (k + 1)] + F[L0][i * J * K + j * K + (k - 1)]) / owz;
		diff = fabs(F[L1][i * J * K + j * K + k] - (Fi + Fj + Fk - Ro((i + offset[threadRank]) * hx, j * hy, k * hz)) / c);
                F[L1][i * J * K + j * K + k] = (Fi + Fj + Fk - Ro((i + offset[threadRank]) * hx, j * hy, k * hz)) / c;
                if (diff > EPSILON) {
                    f = 0;
                }
            }
        }
    }
}

void recData() {
    if (threadRank != 0) {
        MPI_Wait(&recRequest[1], MPI_STATUS_IGNORE);
        MPI_Wait(&sendRequest[0], MPI_STATUS_IGNORE);
    }
    if (threadRank != threadCount - 1) {
        MPI_Wait(&recRequest[0], MPI_STATUS_IGNORE);
        MPI_Wait(&sendRequest[1], MPI_STATUS_IGNORE);
    }
}

void findMaxDiff() {
    double max = 0;
	int i, j, k;
    for (i = 1; i < perThread[threadRank]; i++) {
        for (j = 1; j < jn; j++) {
            for (k = 1; k < kn; k++) {
		F1 = fabs(F[L1][i * J * K + j * K + k] - Phi((i + offset[threadRank]) * hx, j * hy, k * hz));
		printf("F1 = %e\n", F1);
                if (F1 > max) {
                    max = F1;
                }
            }
        }
    }
    double tmpMax = 0;
    MPI_Allreduce(&max, &tmpMax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

    if (threadRank == 0) {
        max = tmpMax;
	printf("in = %d\njn = %d\nkn = %d\n", in, jn, kn);
        printf("Max differ = %e\n", max);
    }
}

int main(int argc, char **argv) {
    MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD, &threadCount);
    MPI_Comm_rank(MPI_COMM_WORLD, &threadRank);

    if (threadRank == 0) {
        printf("Threads: %d\n", threadCount);
    }

    perThread = malloc(sizeof(int) * threadCount);
    offset = malloc(sizeof(int) * threadCount);
	int i, height, tmp, currentLine;;
    for (i = 0, height = kn + 1, tmp = threadCount - (height % threadCount), currentLine = 0; i < threadCount; i++) {
        offset[i] = currentLine;
        perThread[i] = i < tmp ? (height / threadCount) : (height / threadCount + 1);
        currentLine += perThread[i];
    }

    I = perThread[threadRank];
    J = (jn + 1);
    K = (kn + 1);

    F[0] = malloc(sizeof(double) * I * J * K);
    F[1] = malloc(sizeof(double) * I * J * K);

    buffer[0] = malloc(sizeof(double) * K * J);
    buffer[1] = malloc(sizeof(double) * K * J);

    hx = X / in;
    hy = Y / jn;
    hz = Z / kn;

    owx = pow(hx, 2);
    owy = pow(hy, 2);
    owz = pow(hz, 2);
    c = 2 / owx + 2 / owy + 2 / owz + a;

    Init(perThread, offset, threadRank);

    do {
        f = 1;
        L0 = 1 - L0;
        L1 = 1 - L1;

        /*send edges*/
        sendData();
        /*calculate center*/
        calcCenter();
        /*wait of all data*/
        recData();
        /*calculate edges*/
        calcEdges();

        MPI_Allreduce(&f, &tmpF, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);
        f = tmpF;
    } while (f == 0);

    findMaxDiff();

    free(buffer[0]);
    free(buffer[1]);
    free(F[0]);
    free(F[1]);
    free(offset);
    free(perThread);

    MPI_Finalize();
    return 0;
}

