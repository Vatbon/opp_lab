#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define N 14400 /*Size of an array*/
#define E 0.0000001 /*Epsilon*/

double* matrOnVec(const double* matr,const double* vec){
	double* res = malloc(sizeof(double)*N);
	int i,j;
	for(i = 0; i < N; i++){
		res[i] = 0;
		for (j = 0; j < N; j++){
			res[i] += matr[i*N + j] * vec[j];
		}
	}
	return res;
}

double* scalOnVec(const double scal,const double* vec){
	double* res = malloc(sizeof(double)*N);
	int i;
	for(i = 0 ; i < N; i++)
		res[i] = vec[i]*scal; 
	return res;
}

/* res = vec1 - vec2 */
double* vecMinusVec(const double* vec1,const double* vec2){
	double * res = malloc(sizeof(double)*N);
	int i;
	for (i = 0; i < N; i++)
		res[i] = vec1[i] - vec2[i];
	return res;
}

double normOfVec(const double* vec){
	double res = 0;
	int i;
	for (i = 0; i < N; i++)
		res += vec[i]*vec[i];
	return sqrt(res);
}

/*Solving Ax = b
 ** A = N x N matrix of double
 ** x, b vectors of double N-len
 ** x^(n+1) = x^n - t(Ax^n - b)
 ** t = 0.01 or -0.01
 ** |Ax^n - b|/|b| < E - criteria of an exit
 ** |x| = sqrt(sum of x_i^2)
 */

double* SimpleIter(const double* A,const double* b){
	double* res = (double*)malloc(sizeof(double)*N);
	double *x;
	double *_x;
	double *_y;
	double t = 0.0001;
	double z;
	int i;
	for(i = 0 ; i < N; i++)
		res[i] = 0;
	do{
		_x = matrOnVec(A, res);
		_y = vecMinusVec(_x, b);
		free(_x);
		_x = scalOnVec(t, _y);
		x = vecMinusVec(res, _x); /*x^(n+1)*/
		z = normOfVec(_y)/normOfVec(b);
		for (i = 0; i < N; i++){
			res[i] = x[i];
			//printf("%f - %f = %f\n",b[i], res[i], b[i] - res[i]);
		}
		//printf("z = %lf\n\n", z);
		free(x);
		free(_x);
		free(_y);
	}while(z > E);
	return res;
}


int main(){
	double *A =( double*)malloc(sizeof(double)*N*N);
	double *b = (double*)malloc(sizeof(double)*N);
	int i,j;
	srand(2019);
	for(i = 0; i < N; i++)
		for(j = 0; j < N; j++)
			if (i==j)
				A[i*N + j] = 2;
			else
				A[i*N + j] = 1;
	for(i = 0; i < N; i++)
		b[i] = N + 1;
	//printf("%f\n", b[0]);
	/*Init A and b*/
	double *res = SimpleIter(A, b);
	//for (i = 0; i < N; i++)
	//        printf("%f\n", res[i]);
	return 0;
}
