#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/times.h>

#define N 14400 /*Size of an array*/
#define E 0.00000001 /*Epsilon*/



/* res = vec1 - vec2 */


/*Solving Ax = b
 ** A = N x N matrix of double
 ** x, b vectors of double N-len
 ** x^(n+1) = x^n - t(Ax^n - b)
 ** t = 0.01 or -0.01
 ** |Ax^n - b|/|b| < E - criteria of an exit
 ** |x| = sqrt(sum of x_i^2)
 */



int main(){
	struct timeval start, end;
	long clocks;
	double *A =( double*)malloc(sizeof(double)*N*N);
	double *b = (double*)malloc(sizeof(double)*N);
	double* res = (double*)malloc(sizeof(double)*N);
	double *x = (double*)malloc(sizeof(double)*N);
	double *_x = (double*)malloc(sizeof(double)*N);
	double *_y = (double*)malloc(sizeof(double)*N);
	srand(2019);
	gettimeofday(&start, NULL);
	double z, temp;
	double t = 0.0001;
			double z1 = 0, z2 = 0;
		int i,j;
#pragma omp parallel shared(t, A, b, res, x, _x, _y, z) reduction(+:z1) reduction(+:z2) private(i,j)
	{	
		for(i = 0; i < N; i++)
			for(j = 0; j < N; j++)
				if (i==j)
					A[i*N + j] = 2;
				else
					A[i*N + j] = 1;
		for(i = 0; i < N; i++)
			b[i] = N + 1;
		//printf("%f\n", b[0]);
		/*Init A and b*/
		int rank = omp_get_thread_num();

		/*Main Part*/
		for(i = 0 ; i < N; i++)
			res[i] = 0;
#pragma omp barrier
		do{	
			/*matrOnVec(A, res, _x);*/
			for(i = 0; i < N; i++)
				_x[i] = 0;
#pragma omp barrier
			for(i = 0; i < N; i++){
#pragma omp single nowait
				{
					for (j = 0; j < N; j++){
						_x[i] += A[i*N + j] * res[j];
					}
				}
			}
			/*matrOnVec(A, res, _x);*/
#pragma omp barrier
			/*vecMinusVec(_x, b, _y);*/
			for (i = 0; i < N; i++)
				_y[i] = _x[i] - b[i];
			/*vecMinusVec(_x, b, _y);*/

#pragma omp barrier
			/*scalOnVec(t, _y, _x);*/
			for(i = 0 ; i < N; i++)
				_x[i] = _y[i]*t; 
			/*scalOnVec(t, _y, _x);*/

#pragma omp barrier
			/*vecMinusVec(res, _x, x);*/ /*x^(n+1)*/
			for (i = 0; i < N; i++)
				x[i] = res[i] - _x[i];
			/*vecMinusVec(res, _x, x);*/ /*x^(n+1)*/
			z1 = 0;
			z2 = 0;
#pragma omp barrier	
			for (i = 0; i < N; i++){
				z1 += _y[i]*_y[i];
				z2 += b[i]*b[i];
			}
#pragma omp barrier
			z = sqrt(z1/z2);

			for (i = 0; i < N; i++){
				res[i] = x[i];
				//printf("res[%d] = %f\n",i ,res[i]);
			}
			//printf("In thread %d z = %lf\n",rank, z);
		}while(z > E);
		/*End of main part*/
	}
	//for (i = 0; i < N; i++)
	//        printf("%f\n", res[i]);

	gettimeofday(&end, NULL);
	clocks = (end.tv_sec - start.tv_sec) * 1000000 + end.tv_usec - start.tv_usec;
	printf("Time taken on %d threads: %ld microseconds\n",omp_get_max_threads(), clocks);

	return 0;
}
