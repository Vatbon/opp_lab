#!/bin/sh
#PBS -l walltime=00:02:00
#PBS -l select=1:ncpus=2:ompthreads=2:mem=4000m
#PBS -q bl2x220g6q
cd $PBS_O_WORKDIR
echo "Inititally OMP_NUM_THREADS = $OMP_NUM_THREADS"
## Compile with '-trace' parameter to use ITAC:
gcc ~/code/lab2/main_openmpi_full.c -O0 -fopenmp -lm -o prog_openmpfull2
~/code/lab2/prog_openmpfull2
