#!/bin/sh
#PBS -l walltime=00:10:00
#PBS -l select=1:ncpus=5:mpiprocs=5:mem=4000m

cd $PBS_O_WORKDIR

## Set variables for ITAC:
source /opt/intel/itac/8.1.3.037/bin/itacvars.sh

## Set variables for Intel compiler:
source /opt/intel/composerxe/bin/compilervars.sh intel64
export I_MPI_CC=icc

## Compile with '-trace' parameter to use ITAC:
mpicxx -trace ~/code/lab5/main.cpp -o ~/code/lab5/prog5 -mt_mpi

## Count the number of MPI processes:
MPI_NP=`wc -l $PBS_NODEFILE | awk '{ print $1 }'`

## Add '-trace' parameter:
mpirun -trace -machinefile $PBS_NODEFILE -np $MPI_NP ~/code/lab5/prog5

