#include <cstdio>
#include <ctime>
#include <pthread.h>
#include <mpi.h>
#include <cstddef>
#include <cstdlib>
#include <vector>
#include <cstdarg>
#include <cmath>

#define COUNT_TASKS 10

#define REQUEST_TAG 0
#define ANSWER_TAG 1
#define DATA_TAG 2

typedef struct task_t {
    int weight;
} task_t;

int size, rank, cur_task;
MPI_Datatype TASK_TYPE;
pthread_mutex_t task_mutex;
pthread_mutex_t print_mutex;
pthread_t data_thread;
std::vector<task_t> tasks;
long *ops = new long[16];
struct timespec start;

long double do_task(task_t task) {
    long double res = 0;
    for (int i = 0; i< task.weight; i++){
        sqrt(i);
	res++;
    }
    return res;
}

void dev_print(const char *thread_name, const char *str, ...) {
    va_list args;
    va_start (args, str);

    struct timespec end;
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    pthread_mutex_lock(&print_mutex);
    printf("%lf   Process %d[%s]: ", end.tv_sec - start.tv_sec + 0.000000001 * (end.tv_nsec - start.tv_nsec),
            rank, thread_name);
    vprintf(str, args);
    printf("\n");
    fflush(stdout);
    pthread_mutex_unlock(&print_mutex);
    va_end(args);
}

void *calc_thread_func(void *args) {
    dev_print("calc", "started");
    // 1 - может отправить задачи, 0 - нет
    int *proc_state = new int[size];
    for (int i = 0; i < size; i++)
        proc_state[i] = 1;
    proc_state[rank] = 0;

    cur_task = 0;
    int cur_supplier = 0;

    while (cur_supplier < size) {
        pthread_mutex_lock(&task_mutex);
        while (cur_task < tasks.size()) {
            pthread_mutex_unlock(&task_mutex);

            ops[rank] += do_task(tasks[cur_task]);
		
            pthread_mutex_lock(&task_mutex);
            dev_print("calc", "finished task %d/%d", cur_task + 1, tasks.size());
            pthread_mutex_unlock(&task_mutex);

            cur_task++;

            pthread_mutex_lock(&task_mutex);
        }
        pthread_mutex_unlock(&task_mutex);

        int supplier_rank = (rank + cur_supplier) % size;
        if (!proc_state[supplier_rank]) {
            cur_supplier++;
            continue;
        }

        //пробуем получить ещё задание

        dev_print("calc", "trying to get tasks from process %d", supplier_rank);

        int message = 1;
        MPI_Send(&message, 1, MPI_INT, supplier_rank, REQUEST_TAG, MPI_COMM_WORLD);
        MPI_Recv(&message, 1, MPI_INT, supplier_rank, ANSWER_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        if(message == 0) {
            dev_print("calc", "no tasks available in process %d", supplier_rank);
            proc_state[supplier_rank] = 0;
            cur_supplier++;
        }
        else {
            //dev_print("calc", "receiving new task from process %d...", supplier_rank);
            task_t task;
            MPI_Recv(&task, 1, TASK_TYPE, supplier_rank, DATA_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            dev_print("calc", "received new task from process %d", supplier_rank);

            pthread_mutex_lock(&task_mutex);
            tasks.push_back(task);
            pthread_mutex_unlock(&task_mutex);

            //dev_print("calc", "put to list new task from process %d", supplier_rank);
            //proc_state[supplier_rank] = 1;
        }
    }

    //ждём все процессы
    dev_print("calc", "finished iteration, waiting for others");
    MPI_Barrier(MPI_COMM_WORLD);

    //отправляем в соседний поток чтобы он завершился
    int message = 0;
    MPI_Send(&message, 1, MPI_INT, rank, REQUEST_TAG, MPI_COMM_WORLD);

    delete[] proc_state;
    dev_print("calc", "ended");
    return 0;
}

void *data_thread_function(void *args) {
    int message, answer;
    MPI_Status status;
    while (true) {
        MPI_Recv(&message, 1, MPI_INT, MPI_ANY_SOURCE, REQUEST_TAG, MPI_COMM_WORLD, &status);

        //dev_print("data", "received message %d from process %d", message, status.MPI_SOURCE);

        if (message == 0)
            break;

        pthread_mutex_lock(&task_mutex);

        if (cur_task >= tasks.size() - 1) {
            answer = 0;
            MPI_Send(&answer, 1, MPI_INT, status.MPI_SOURCE, ANSWER_TAG, MPI_COMM_WORLD);

            pthread_mutex_unlock(&task_mutex);

            //dev_print("data", "don't have tasks for process %d", status.MPI_SOURCE);
            continue;
        }

        answer = 1;
        MPI_Send(&answer, 1, MPI_INT, status.MPI_SOURCE, ANSWER_TAG, MPI_COMM_WORLD);
        MPI_Send(&tasks.back(), 1, TASK_TYPE, status.MPI_SOURCE, DATA_TAG, MPI_COMM_WORLD);
        tasks.pop_back();

        //dev_print("data", "sent task to process %d", status.MPI_SOURCE);

        pthread_mutex_unlock(&task_mutex);
    }

    dev_print("data", "ended");
    return 0;
}

int main(int argc, char **argv) {
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    int provided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

    if (provided != MPI_THREAD_MULTIPLE) {
        fprintf(stdout, "Can't get MPI_THREAD_MULTIPLE (%d) level, got %d\n", MPI_THREAD_MULTIPLE, provided);
        MPI_Finalize();
        return 1;
    }

    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    //создаём MPI тип

    int fields_number = 1;
    int block_lengths[] = {1};
    MPI_Datatype types[] = {MPI_INT}, temp_type;
    MPI_Aint offsets[] = {offsetof(task_t, weight)}, lb, extent;

    MPI_Type_create_struct(fields_number, block_lengths, offsets, types, &temp_type);
    MPI_Type_get_extent(temp_type, &lb, &extent);
    MPI_Type_create_resized(temp_type, lb, extent, &TASK_TYPE);
    MPI_Type_commit(&TASK_TYPE);

    //генерируем задания
    for (int i = 0; i < size; i++)
        ops[i] = 0;
    dev_print("calc", "started task generation");
    int weight = (rank + 1) * 10000000;
    struct timespec end;
    for (int i = 0; i < size; i++){
if (rank == i){
        clock_gettime(CLOCK_MONOTONIC_RAW, &end);
            printf("%lf: Process %d(%s): Init amunt of operations equals %ld\n", end.tv_sec - start.tv_sec + 0.000000001 * (end.tv_nsec - start.tv_nsec),
                    rank, "main", weight * COUNT_TASKS);
}
	MPI_Barrier(MPI_COMM_WORLD);
    }
    for (int i = 0; i < COUNT_TASKS; i++) {
        task_t task;
        task.weight = weight;
        tasks.push_back(task);
    }
    dev_print("calc", "finished task generation");

    //создаём мьютекс и второй тред
    pthread_mutex_init(&task_mutex, NULL);
    pthread_mutex_init(&print_mutex, NULL);

    pthread_attr_t attrs;
    pthread_attr_init(&attrs);
    pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_JOINABLE);
    pthread_create(&data_thread, &attrs, data_thread_function, NULL);   //создаём ожидающий запросы поток

    calc_thread_func(NULL);     //основной поток будет считать

    if (pthread_join(data_thread, NULL) != EXIT_SUCCESS) {
        perror("Cannot join a thread");
    }
    for (int i = 0; i < size; i++){
if (rank == i){
        clock_gettime(CLOCK_MONOTONIC_RAW, &end);
            printf("%lf: Process %d(%s): Process %d made %ld operations\n", end.tv_sec - start.tv_sec + 0.000000001 * (end.tv_nsec - start.tv_nsec),
                    rank, "main", i, ops[i]);
}
	MPI_Barrier(MPI_COMM_WORLD);
    }
    MPI_Finalize();
    return 0;
}
